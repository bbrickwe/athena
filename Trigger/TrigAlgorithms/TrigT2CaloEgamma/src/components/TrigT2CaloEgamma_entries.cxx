#include "../T2CaloEgammaReFastAlgo.h"
#include "../EgammaReSamp2Fex.h"
#include "../EgammaReSamp1Fex.h"
#include "../EgammaReEmEnFex.h"
#include "../EgammaReHadEnFex.h"
#include "../RingerReFex.h"
#include "../EgammaAllFex.h"

DECLARE_COMPONENT( T2CaloEgammaReFastAlgo )
DECLARE_COMPONENT( EgammaReSamp2Fex )
DECLARE_COMPONENT( EgammaReSamp1Fex )
DECLARE_COMPONENT( EgammaReEmEnFex )
DECLARE_COMPONENT( EgammaReHadEnFex )
DECLARE_COMPONENT( RingerReFex )
DECLARE_COMPONENT( EgammaAllFex )
