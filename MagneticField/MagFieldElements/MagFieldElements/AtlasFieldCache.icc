/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
inline MagField::AtlasFieldCache::AtlasFieldCache(double solFieldScale,
                                                  double torFieldScale,
                                                  const AtlasFieldMap* fieldMap)
  : m_solScale(solFieldScale)
  , m_torScale(torFieldScale)
  ,
  // set field service
  m_fieldMap(fieldMap)

{
  if (m_fieldMap) {
    // save ZR bfield
    m_meshZR = m_fieldMap->getBFieldMesh();
    // Get solenoid zone id from field service
    m_solZoneId = fieldMap->solenoidZoneId();
  }
}

inline bool
MagField::AtlasFieldCache::fillFieldCache(double z, double r, double phi)
{
  // search for the zone
  const BFieldZone* zone =
    m_fieldMap ? m_fieldMap->findBFieldZone(z, r, phi) : nullptr;

  if (!zone) {
    // outsize all zones
    return false;
  }

  // set scale for field
  m_scaleToUse = (zone->id() == m_solZoneId) ? m_solScale : m_torScale;

  // fill the cache, pass in current scale factor
  zone->getCache(z, r, phi, m_cache3d, m_scaleToUse);

  // save pointer to the conductors in the zone
  m_cond = zone->condVector();

  return true;
}

inline bool
MagField::AtlasFieldCache::fillFieldCacheZR(double z, double r)
{
  // No mesh available
  if (!m_meshZR) {
    return false;
  }
  // Not inside the solenoid zone?
  if (!m_meshZR->inside(z, r)) {
    return false;
  }
  // fill the cache, pass in current scale factor
  m_meshZR->getCache(z, r, m_cacheZR, m_solScale);
  return true;
}

inline bool
MagField::AtlasFieldCache::solenoidOn() const
{
  return m_fieldMap ? m_fieldMap->solenoidOn() && m_solScale > 0.0 : false;
}

inline bool
MagField::AtlasFieldCache::toroidOn() const
{
  return m_fieldMap ? m_fieldMap->toroidOn() && m_torScale > 0.0 : false;
}
